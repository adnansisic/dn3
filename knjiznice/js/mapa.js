var mapa;
var L = window.L;
var a, c, z, p, q;
var counter;

window.addEventListener('load', function () {
  // Osnovne lastnosti mape
  var mapOptions = {
    center: [46.056946, 14.505751],
    zoom: 12
  };

  mapa = new L.map('mapa_id', mapOptions);

  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  mapa.addLayer(layer);

  var popup = L.popup();

  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
    popup
      .setLatLng(latlng)
      .setContent("Izbrana točka:" + latlng.toString())
      .openOn(mapa);
  }

  mapa.on('click', obKlikuNaMapo);
  
  var coordsForPolygon = [];
  var coordsForMarkers = [];
  var namesForPolygon = [];
  $.getJSON('https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json', function(json){
    for(var key in json) {
      if(json.hasOwnProperty(key)) {
        var item = json[key];
      }
    }
    
    for(var i = 0; i < item.length; i++){
      //console.log(json.features[i].properties.addr:street);
      //console.log(json.features[i].properties.addr:city);
      
      a = json.features[i].geometry.coordinates;
      c = a[1];
      
      if(a.length == 2 && c.length > 0){
        
        z = json.features[i].geometry.coordinates;
        p = z[0];
        q = z[1];
        
        for(var j = 0; j < p.length; j++){
          [(json.features[i].geometry.coordinates)[0][j][0], 
           (json.features[i].geometry.coordinates)[0][j][1]] =
          [(json.features[i].geometry.coordinates)[0][j][1], 
           (json.features[i].geometry.coordinates)[0][j][0]];
        }
        
        for(var k = 0; k < q.length; k++){
          [(json.features[i].geometry.coordinates)[1][k][0], 
           (json.features[i].geometry.coordinates)[1][k][1]] =
          [(json.features[i].geometry.coordinates)[1][k][1], 
           (json.features[i].geometry.coordinates)[1][k][0]];
        }
        
        coordsForPolygon.push(json.features[i].geometry.coordinates);
        namesForPolygon.push(json.features[i].properties.name);
        
      }else if(a.length == 1){
        
        for(var u = 0; u < a[0].length; u++){
          [(json.features[i].geometry.coordinates)[0][u][0], 
           (json.features[i].geometry.coordinates)[0][u][1]] =
          [(json.features[i].geometry.coordinates)[0][u][1], 
           (json.features[i].geometry.coordinates)[0][u][0]];
        }
        
        coordsForPolygon.push(json.features[i].geometry.coordinates);
        
      }else if(a.length == 2){
        [(json.features[i].geometry.coordinates)[0], 
         (json.features[i].geometry.coordinates)[1]] =
        [(json.features[i].geometry.coordinates)[1], 
         (json.features[i].geometry.coordinates)[0]];
        coordsForMarkers.push(json.features[i].geometry.coordinates);
      }
    }
    
    dodajMarker();
    
    var polygon = L.polygon(coordsForPolygon, {color: 'blue'}).addTo(mapa);
    mapa.fitBounds(polygon.getBounds());
  });
  
  function dodajMarker(){
    var blueIcon = L.icon({
      iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
        'marker-icon-2x-blue.png',
      //shadowUrl: 'leaf-shadow.png',
    
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34]
    });
    
    var marker;
    
    for(var o = 0; o < coordsForMarkers.length; o++){
      L.marker(coordsForMarkers[o], {icon: blueIcon}).addTo(mapa);
      //marker.bindPopup("<div>Naziv: " + opis + "</div>").openPopup();
    }
  }
});