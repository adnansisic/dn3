/* global $*/
/* global CanvasJS */
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
var heights = [];
var weights = [];

var dataWeights = [];
var dataHeights = [];

var ime;
var priimek;
var datumRojstva;
var doctor;
var nurse;
var ehrId;
var chart;

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
 
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 */

function generirajPodatke() {
  for(var i = 0; i < 3; i++){
	$.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (data) {
      ime = randomIme();
    	priimek = randomPriimek();
      datumRojstva = randomDatum();
      doctor = randomDoctor();
      nurse = randomMerilec();
      ehrId = data.ehrId;
      
      var partyData = {
        firstNames: ime,
        lastNames: priimek,
        dateOfBirth: datumRojstva,
        additionalInfo: {"ehrId": ehrId, "doctor": doctor, "nurse": nurse}
      };
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        headers: {
          "Authorization": getAuthorization()
        },
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        error: function(err) {
        	$("#kreirajSporocilo").html("<span class='obvestilo label " +
            "label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
        }
      });
      alert("Uspešno kreiran EHR '" + ehrId + "'");
      
      //ZA TESTIRANJE
      /*
      console.log("Kreiranje novega bolnika:");
      console.log("   " + ime);
      console.log("   " + priimek);
      console.log("   " + datumRojstva);
      console.log("   " + doctor);
      console.log("   " + ehrId);
      console.log("############################################");
      */
      
      for(var i = 0; i < 5; i++){
        generiranjePodatkov();
      }
      add();
    }
	});
  }
}
	
function generiranjePodatkov() {
  
	var datumInUra = randomDatumInUra();
	var telesnaVisina = randomVisina();
	var telesnaTeza = randomTeza();
	  
	  //ZA TESTIRANJE
	  /*
	  console.log("   " + datumInUra);
	  console.log("   " + telesnaVisina);
	  console.log("   " + telesnaTeza);
	  console.log("############################################");
	  */
		
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      error: function(err) {
      	alert(JSON.parse(err.responseText).userMessage + "!");
      }
	});
}

function add() {
  var ddl = document.getElementById("preberiOsnovnePodatke");
  var option = document.createElement("OPTION");
  option.innerHTML = (ime + " " + priimek);
  option.value = (ehrId + "|" + ime + "|" + priimek + "|" + datumRojstva);
 ddl.options.add(option);
}

function preberiEHRodBolnika() {
	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		alert("Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			$("#podatekIme").val(party.firstNames);
  			$("#podatekPriimek").val(party.lastNames);
  			$("#podatekEHR").val(ehrId);
  			$("#podatekDatumRojstva").val(party.dateOfBirth);
  			$("#podatekImePriimek").val(party.firstNames + " " + party.lastNames);
  			//getDoctor = party.additionalInfo.doctor;
  			//console.log(party.additionalInfo.doctor);
  			
  		},
  		error: function(err) {
  			alert("Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
	}
}

function graf() {
  var ime = document.getElementById("podatekIme").value;
  var priimek = document.getElementById("podatekPriimek").value;
  
  var weightsResult = weights;
  var heightsResult = heights;
  
  dataWeights = [];
  dataHeights = [];
  
  for (var i = 0; i < weightsResult.length; i++) {
    dataWeights.push({ x: Number(i+1), y: Number(weightsResult[i].y) });
  }
  
  for (var i = 0; i < heightsResult.length; i++) {
    dataHeights.push({ x: Number(i+1), y: Number(heightsResult[i].y) });
  }
  
  var visitorsData = {
  	"Meritve vitalnih znakov": [{
  		click: visitorsChartDrilldownHandler,
  		cursor: "pointer",
  		explodeOnClick: false,
  		innerRadius: "75%",
  		legendMarkerType: "square",
  		name: "Meritve vitalnih znakov za:  " + ime + " " + priimek,
  		radius: "100%",
  		showInLegend: true,
  		startAngle: 90,
  		type: "doughnut",
  		dataPoints: [
  			{ y: 1, name: "Telesna teža", color: "#E7823A" },
  			{ y: 1, name: "Telesna višina", color: "#546BC1" }
  		]
  	}],
  	"Telesna teža": [{
  	  click: moreInfo,
  		color: "#E7823A",
  		name: "Telesna teža",
  		type: "column",
  		dataPoints: dataWeights
  	}],
  	"Telesna višina": [{
  	  click: moreInfo,
  		color: "#546BC1",
  		name: "Telesna višina",
  		type: "column",
  		dataPoints: dataHeights
  	}]
  };
  
  var newVSReturningVisitorsOptions = {
  	animationEnabled: true,
  	theme: "light2",
  	title: {
  		text: "Meritve vitalnih znakov za:  " + ime + " " + priimek
  	},
  	subtitles: [{
  		text: "Izberite stran",
  		backgroundColor: "#2eacd1",
  		fontSize: 16,
  		fontColor: "white",
  		padding: 5
  	}],
  	legend: {
  		fontFamily: "calibri",
  		fontSize: 14,
  		itemTextFormatter: function (e) {
  			return e.dataPoint.name;  
  		}
  	},
  	data: []
  };
  
  var visitorsDrilldownedChartOptions = {
  	animationEnabled: true,
  	theme: "light2",
  	axisX: {
  		labelFontColor: "#717171",
  		lineColor: "#a2a2a2",
  		tickColor: "#a2a2a2"
  	},
  	axisY: {
  		gridThickness: 0,
  		includeZero: false,
  		labelFontColor: "#717171",
  		lineColor: "#a2a2a2",
  		tickColor: "#a2a2a2",
  		lineThickness: 1
  	},
  	data: []
  };
  
  chart = new CanvasJS.Chart("chartContainer", newVSReturningVisitorsOptions);
  chart.options.data = visitorsData["Meritve vitalnih znakov"];
  chart.render();
  
  function visitorsChartDrilldownHandler(e) {
  	chart = new CanvasJS.Chart("chartContainer", visitorsDrilldownedChartOptions);
  	chart.options.data = visitorsData[e.dataPoint.name];
  	chart.options.title = { text: e.dataPoint.name };
  	chart.render();
  	$("#pojasnilo").text("X: zaporedno število meritve | Y: merilna enota (kg / cm)");
  	$("#pojasnilo").fadeIn(200).fadeOut(200).fadeIn(200).fadeOut(200).fadeIn(200).fadeIn(200).fadeOut(200).fadeIn(200).fadeOut(200).fadeIn(200);
  	$("#backButton").removeClass("invisible");
  }
  
  $("#backButton").click(function() {
    var element = document.getElementById("backButton");
    element.classList.toggle("invisible");
  	chart = new CanvasJS.Chart("chartContainer", newVSReturningVisitorsOptions);
  	chart.options.data = visitorsData["Meritve vitalnih znakov"];
  	chart.render();
  });
  
  function moreInfo(){
    var ehrId = $("#podatekEHR").val();
    
  	$.ajax({
  		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
  		type: 'GET',
  		headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
    		var party = data.party;
    		document.getElementById("podatekDoktor").value = party.additionalInfo.doctor;
    		document.getElementById("podatekMerilec").value = party.additionalInfo.nurse;
    	},
    	error: function(err) {
    		alert("Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
    	}
  	});
  	$("#popup").show();
  }
}

function preberiMeritveVitalnihZnakov() {
  var ehrId = $("#podatekEHR").val();
	if (!ehrId || ehrId.trim().length == 0) {
		alert("Prosim preverite EHR ID v osnovnih podatkih!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				  var resultH = [];
  					$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "height",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
  				        for (var i in res) {
      		          resultH.push({x: Number(i), y: Number(res[i].height)});
  				        }
  				        heights = resultH;
  				        document.getElementById("chartContainer").html = graf();
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  					
  				  var resultW = [];
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
  				        for (var i in res) {
      		          resultW.push({x: Number(i), y: Number(res[i].weight)});
  				        }
  				        weights = resultW;
  				        document.getElementById("chartContainer").html = graf();
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				},
      	error: function(err) {
      		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
      	}
  	});
	}
}

function randomVisina(){
  var v = ((Math.random() * (10) ) + 170).toFixed(1);
  return v;
}

function randomTeza(){
  var t = ((Math.random() * (50) ) + 60).toFixed(1);
  return t;
}
  
function randomDatumInUra() {
  var year = 2019;
  var time = "04:20:00";
	var month = Math.floor(Math.random() * (4) ) + 1;
	var day = Math.min(days[month-1], Math.max(1, Math.round(Math.random() * 31)));
	return year + "-0" + month  + "-" + (day < 10 ? "0" + day : day) + "T" + time;
}

$(document).ready(function() {
	$('#preberiOsnovnePodatke').change(function() {
		var podatki = $(this).val().split("|");
		$("#podatekEHR").val(podatki[0]);
		$("#podatekIme").val(podatki[1]);
		$("#podatekPriimek").val(podatki[2]);
		$("#podatekDatumRojstva").val(podatki[3]);
		$("#podatekImePriimek").val(podatki[1] + " " + podatki[2]);
	});
});

function vrniTezo() {
  return (dataWeights[dataWeights.length - 1].y);
}

function vrniVisino() {
  return (dataHeights[dataHeights.length - 1].y);
}

function calculateBmiBolnika() {
  var weight = vrniTezo();
  var height = vrniVisino();
  
  var finalBmi = (weight / (height / 100 * height / 100));
  return finalBmi;
}

function calculateBmiRocno() {
  var weight = document.getElementById("dodajRocnoTeza").value;
  var height = document.getElementById("dodajRocnoVisina").value;
  
  var finalBmi = (weight / (height / 100 * height / 100));
  return finalBmi;
}

function randomIme() {
	var name = ["Lara", "Anja", "Jan", "Ana", "Luka", "Mark", "Tomas", "Gregor", "Jaka", "Marija", "Nika", "Gabrijel", "Karmen", "Neža"];
	var i = Math.floor(Math.random() * name.length);
	return name[i];
}

function randomPriimek() {
	var surname = ["Novak", "Horvat", "Kovačič", "Krajnc", "Zupančič", "Potočnik", "Kovač", "Mlakar", "Kos", "Vidmar", "Košir", "Klemenčič", "Medved", "Zajc"];
	var i = Math.floor(Math.random() * surname.length);
	return surname[i];
}

function randomDoctor() {
  var doctor = ["Ivana Črepnjak, dr. med.", "Jovica Vajdec, dr. med.", "Petronela Španbauer, dr. med.", "Lorenc Jaksetič, dr. med.", "Jona Loparič, dr. med.", "Lovrenc Žepič, dr. med.", "Virna Lipar, dr. med.", "Lota Edelsbruner, dr. med."];
	var i = Math.floor(Math.random() * doctor.length);
	return doctor[i];
}

function randomMerilec() {
  var merilec = ["Med. sestra Evita Mohar", "Med. sestra Marjanca Janc", "Med. sestra Magdalena Doberšek", "Med. sestra Bogdana Jelen", "Med. sestra Suzana Žohar", "Med. sestra Aljana Gobec", "Med. sestra Adrijana Špes", "Med. sestra Blažka Orel"];
	var i = Math.floor(Math.random() * merilec.length);
	return merilec[i];
}

function randomDatum() {
	var year = 1940 + Math.round(Math.random() * 60);
	var month = 1 + Math.round(Math.random() * 11);
	var day = Math.min(days[month-1], Math.max(1, Math.round(Math.random() * 31)));
	return(year + "-" + (month  < 10 ? "0" + month : month )  + "-" + (day < 10 ? "0" + day : day));
}